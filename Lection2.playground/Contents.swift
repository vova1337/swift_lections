import UIKit

// Optionals

var someOpt: Int? = 5

// force unwrapping

if someOpt != nil {
    print("someOpt = \(someOpt!)")
}else {
    print("someOpt is nil")
}


// optional binding

if var someOpt = someOpt {
        someOpt += 5
}else {
    print("someOpt is nil")
}


// Implicitly unwrapped optionals

var impOpt: Int!

impOpt = 5
print(impOpt)


////////////////////////////////////

// Typealias

typealias name = String
var userName: name = "Ivan"


////////////////////////////////////

//Tupels

var adress = ("Drogobych", "V.Velykogo", 3)

var adress2 = (city: "Drogobych", street: "V.Velykogo", houseNumber: 3)

print("House number is \(adress2.houseNumber)")

adress2.houseNumber = 44

print("House number is \(adress2.houseNumber)")

adress2.street = ""

print("Street is \(adress2.street)")

adress = adress2

print(adress.2)

var (city, street, houseNumber) = adress2
city
street
houseNumber

/////////////////////////////////////


